const functions = require('./index');

test('2 + 2 = 4', () =>{
   expect(functions.sum(2, 2)).toBe(4);
});

test('2 + 2 = 4', () =>{
    expect(functions.sum(2, 2)).not.toBe(5);
});

test('Substr test', () =>{
    expect(functions.mySubstr('Hello', 3)).toBe('Hel');
});

test('Returns null for odd numbers', () => {
    expect(functions.isNull(1)).toBeNull();
    expect(functions.isNull(7)).toBeNull();
});

test('Returns 5 for even numbers', () => {
    expect(functions.isNull(2)).toBe(5);
    expect(functions.isNull(4)).toBe(5);
});

test('2.15 + 1.1 = 3.25', () =>{
   expect(functions.sum(2.15, 1.1)).toBeCloseTo(3.2, 1);
});