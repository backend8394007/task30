const sum = (a, b) => a + b;

const sumStr = (str1, str2) => str1 + ' ' + str2;

const mySubstr = (str, len) => {
    let index = 0;
    let subStr = '';
    while (index < len) {
        subStr = `${subStr}${str[index]}`;
        index++;
    }
    return subStr;
}

const isNull = (num) => {
    if (num % 2 === 0) return 5;
    return null;
}

const functions = {sum, sumStr, mySubstr, isNull};

module.exports = functions;
